# ioprovmeta description for contributors file
- Authors: Jan Buschmann, Benjamin Fuchs, Felix Nitsch
- Contact: benjamin.fuchs@dlr.de
- Version: 1.0.0 

Contributing to ioprocmeta
============================


Thanks for your interest in contribution to ioprocmeta.

First lets get some legal business out of the way that is unfortunately necessary.

We kindly ask you to sign the contributors license agreement as provided with this repository.
The purpose of this document is the clarification of distribution and relicensing rights, which remain with DLR.

After signing and sending us the CLA, you can contribute by gitlab issues followed by a merge request. 
If you want to provide your own meta data format implementation, we kindly ask you to contact us in advance.
Maybe somebody is already working on it or there are some development schedules you should be aware of.

Thank you very much for your contribution and have a good time coding and using ioprocmeta.
